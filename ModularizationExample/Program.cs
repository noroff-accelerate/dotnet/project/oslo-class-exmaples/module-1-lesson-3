﻿using System;

namespace ModularizationExample
{
    class Program
    {       
        static void Main(string[] args)
        {
            // Calling a method with no return
            MyMethod();
            double myDouble = 20;
            // Show overloaded calcualte methods
            Add(3.5, 2,56,2,547,2);
            // Show scope
            // Demonstrate value and reference with primitives
            double total = Factorial(5);
            // Demonstrate value and reference with complex object
            Add(ref myDouble);
            Console.WriteLine(myDouble);
            // Demonstrate type dublication of complex objects
            Person man = new Person() { name = "John" };
            MesswithPeople(man);
            Console.WriteLine(man.name);
            // Demonstrate recursive function
        }

        #region Static method explanation
        // Declare static Prompt method, no return
        // Static allows one copy to be shared accross all instnaces
        /* 1. A static method can be invoked directly from the class level
         *      ClassName.MemberName
         * 2. A static method does not require any class object
         * 3. Any main() method is shared through the entire class scope so it always appears with static keyword.
         * 4. A static class remains in memory for the lifetime of the application domain in which your program resides.
         */
        public static void MyMethod()
        {
            // Do something
        }
        #endregion

        #region Value and reference
        public static void MesswithPeople(Person person)
        {
            Person anotherPerson = new Person();
            anotherPerson = person;
            anotherPerson.name = "Changed";
        }
        #endregion

        #region Overloaded methods
        // Show Calculate for int, double, params
        public static double Add(double number1, double number2)
        {
            return number1 + number2;
        }
        public static double Add(params double[] numbers)
        {
            double total = 0;

            foreach(double number in numbers)
            {
                total += number;
            }
            return total;
        }
        public static double Add(ref double number)
        {
            number += 100;
            return 20;
        }
        #endregion

        #region Scope
        // Demonstrate scope
        // 3 types - class, method block
        static int myGlobal = 1;
        // Global variable accessable to any method.
        // Class level scope
        
        public static void ScopeShow()
        {
            // Local variable in method, accessable inside this method.
            // Method level scope
            int myLocal = 1;
            for (int i = 0; i < 10; i++)
            {
                // Variable declared in {}, only usable within these {}
                // Block level scope
            }
        }
        #endregion

        #region Recursion
        // Simply a method that calls itself
        public static double Factorial(double number)
        {
            if (number == 0)
                return 1;
            return number * Factorial(number - 1);//Recursive call    

        }
        #endregion
    }

    #region Person class
    class Person
    {
        public string name;
        private int height;
    }
    #endregion
}
